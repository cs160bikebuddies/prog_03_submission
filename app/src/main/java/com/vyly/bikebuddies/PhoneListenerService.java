package com.vyly.bikebuddies;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class PhoneListenerService extends WearableListenerService {
    public static final String START_ACTIVITY = "/start_activity";
    public static final String END_RIDE = "/end_ride";
    public static final String MANUAL_MAP = "/manual_list";
    private int notificationId = 001;
    private static int count = 0;
    private String TAG = "WatchLS";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals(END_RIDE)) {
            Log.v(TAG, "ACK");

            Intent intent = new Intent(this, RideList.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
        }
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        DataMap dataMap;
        for (DataEvent event : dataEvents) {
            // Check the data type
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                // Check the data path
                String path = event.getDataItem().getUri().getPath();
                if (path.equals(START_ACTIVITY)) {
                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                    Log.v(TAG, "DataMap received on phone: " + dataMap);
                }
                else if(path.equals(MANUAL_MAP)){


                }


            }
        }
    }



}
