package com.vyly.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity implements View.OnClickListener{

    private GestureDetectorCompat mDetector;
    private String TAG = "MainActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageButton home = (ImageButton)findViewById(R.id.imageButton);
        home.setOnClickListener(MainActivity.this);
        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
//                Toast.makeText(MainActivity.this, "swipeLeft", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeTop() {
//                Toast.makeText(MainActivity.this, "top", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeRight() {
//                Toast.makeText(MainActivity.this, "right", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(MainActivity.this, UserProfile.class);
                startActivity(myIntent);
            }

            public void onSwipeBottom() {
//                Toast.makeText(MainActivity.this, "bottom", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        Log.i(TAG,"onclick");
        Intent myIntent = new Intent(MainActivity.this, UserCreateProfile.class);
        startActivity(myIntent);

    }
}