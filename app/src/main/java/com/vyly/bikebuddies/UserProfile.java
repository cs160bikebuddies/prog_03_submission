package com.vyly.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class UserProfile extends AppCompatActivity implements View.OnClickListener{
    private String TAG = "UserProfile";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        ImageButton home = (ImageButton)findViewById(R.id.userButton);
        home.setOnClickListener(UserProfile.this);
        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
//                Toast.makeText(UserProfile.this, "swipeLeft", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeTop() {
//                Toast.makeText(UserProfile.this, "top", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeRight() {
//                Toast.makeText(UserProfile.this, "right", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(UserProfile.this, GroupFind.class);
                startActivity(myIntent);
            }

            public void onSwipeBottom() {
//                Toast.makeText(UserProfile.this, "bottom", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        Log.i(TAG, "onclick");
        Intent myIntent = new Intent(UserProfile.this,RideCreate.class);
        startActivity(myIntent);
    }
}
