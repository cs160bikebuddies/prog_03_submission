package com.kevin.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends WearableActivity {

    private BoxInsetLayout mContainerView;
    private ImageButton home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContainerView = (BoxInsetLayout) findViewById(R.id.container);
        ImageButton home = (ImageButton)findViewById(R.id.imageButton);

        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(MainActivity.this, MetricsActivity1.class);
                startActivity(myIntent);
            }
        });
    }
}
