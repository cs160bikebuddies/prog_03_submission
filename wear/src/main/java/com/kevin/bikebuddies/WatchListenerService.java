package com.kevin.bikebuddies;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;


public class WatchListenerService extends WearableListenerService {
    public static final String START_RIDE_ACTIVITY = "/start_ride_service";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase( START_RIDE_ACTIVITY ) ) {
            Intent intent = new Intent(this, MetricsActivity1.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            super.onMessageReceived( messageEvent );
        }
    }
}
