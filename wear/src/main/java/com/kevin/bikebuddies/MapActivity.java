package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

public class MapActivity extends Activity {

    private ImageButton map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ImageButton map = (ImageButton) findViewById(R.id.imageButton);
        map.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MapActivity.this, FinishActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
