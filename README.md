# README #

## CS160 Fall 2015 | Group 37: OG WatchMen | BikeBuddies ##
Members: Chris Hsu, Kevin Lu, Matthew Leong, Shreya Chaganti, Vy Ly

### What is this repository for? ###

* This repository contains the code for both the Android phone and watch apps as well as our backend server.  Development is in 2 separate repos and is merged into this repo for easier Prog 03 grading.
* Programming Challenge 1: The code that handles getting biometrics from the watch sensors is in the /wear/src/main/java/com/kevin/bikebuddies/BiometricsService.java directory of this project.
* Programming Challenge 2: The code for the server that handles the data in the backend including the users and the groups is in the api_endpoint folder.


### How do I get set up? ###

If you want to deploy the backend server, we have a separate repo containing just the Rails code; it'll make it a lot easier to push and for us to deploy.  Just ask for access.

Setup instructions will be added soon.

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions