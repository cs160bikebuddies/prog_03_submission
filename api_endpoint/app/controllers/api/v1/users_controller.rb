class Api::V1::UsersController < ApplicationController
	#GET /api/v1/users
	def index
		@user = User.all
		render json: @user
	end

	#GET /api/v1/users/:username/:password/valid
	def validate
		print params[:username]
		@user = User.find_by_username(params[:username])
		render json: {valid: @user.password == params[:password] ? true : false}
	end

	#POST /api/v1/users
	def create
		@user = User.new(create_params)
		@user.save
		render json: @user
	end

	#GET /api/v1/users/:id/edit
	def edit
		render json: {status: "Success"}
	end

	#PUT /api/v1/users/:id/edit
	def update
	end

	#DELETE /api/v1/users/:id
	def destroy
		@user = User.find(params[:id])
		@user.destroy!
		render json: @user
	end

	#GET /api/v1/users/:id
	def show
		@user = User.find(params[:id])
		@groups = @user.groups
		render json: {user: @user, groups: @groups}
	end

	#POST /api/v1/users
	def add_to_group
		@user = User.find(params[:id])
		@groups = User.find(params[:id])
		@groups.users << @user
	end

	#GET /api/v1/users/:id/rides
	def rides
		@user = User.find(params[:id])
		@rides = @user.rides
		render json: @rides
	end

	private
	def create_params
		params.require(:name)
  	end

  	def update_params
		params.require(:user).permit(:name)
  	end
end
