class Api::V1::GroupsController < ApplicationController
	#GET /api/v1/groups
	def index
		@group = Group.all
		render json: @group
	end

	#POST /api/v1/groups
	def create
		@user = User.new(create_params)
		@user.save
		render json: @user
	end

	#GET /api/v1/groups/:id
	def show
		@group = Group.find(params[:id])
		render json: @group
	end

	#GET /api/v1/groups/:id/users
	def users
		@group = Group.find(params[:id])
		render json: @group.users
	end

	private
	def create_params
		params.require(:name)
  	end

  	def update_params
		params.require(:user).permit(:name)
  	end
end
