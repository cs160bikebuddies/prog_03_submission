class User < ActiveRecord::Base
  has_many :memberships
  has_many :ride_metrics
  has_many :groups, through: :memberships
  has_many :rides, through: :ride_metrics
end
