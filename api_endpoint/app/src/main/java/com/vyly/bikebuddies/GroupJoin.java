package com.vyly.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

public class GroupJoin extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_join);
        ImageButton home = (ImageButton)findViewById(R.id.groupJoinButton);
        home.setOnClickListener(GroupJoin.this);

        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
//                Toast.makeText(GroupJoin.this, "swipeLeft", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeTop() {
//                Toast.makeText(GroupJoin.this, "top", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeRight() {
//                Toast.makeText(GroupJoin.this, "right", Toast.LENGTH_SHORT).show();
//                Intent myIntent = new Intent(GroupJoin.this, GroupList.class);
//                startActivity(myIntent);
            }

            public void onSwipeBottom() {
//                Toast.makeText(GroupJoin.this, "bottom", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onClick(View v) {
        Intent myIntent = new Intent(GroupJoin.this,GroupList.class);
        startActivity(myIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_join, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
