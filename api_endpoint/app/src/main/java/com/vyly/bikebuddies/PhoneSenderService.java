package com.vyly.bikebuddies;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

public class PhoneSenderService extends WearableListenerService{
    public static final String START_RIDE_ACTIVITY = "/start_ride_service";
    private static final String TAG = "PhoneSenderService";

    private GoogleApiClient mApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    sendMessage(START_RIDE_ACTIVITY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        return START_NOT_STICKY;
    }



    private void sendNotification(Bundle info){
        Log.i(TAG,"sending datamap");
        mApiClient.connect(); //connect to the API client to send a message!
        sendDataMap(info); //actually send the message to the phone

    }

    private void sendMessage( final String path) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for (Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, null ).await();
                }
            }
        }).start();
    }


    private void sendDataMap(Bundle info){

        // Create a DataMap object and send it to the data layer
        DataMap dataMap = DataMap.fromBundle(info);
        //Requires a new thread to avoid blocking the UI
        try {

            if (info.getString("type").equals(START_RIDE_ACTIVITY)) {
                Log.i(TAG, "sending map activity");
                new SendToDataLayerThread(START_RIDE_ACTIVITY, dataMap).start();
            }  else {

            }
        }catch (Exception e){

            e.printStackTrace();
        }
    }

    class SendToDataLayerThread extends Thread {
        String path;
        DataMap dataMap;

        // Constructor for sending data objects to the data layer
        SendToDataLayerThread(String p, DataMap data) {
            path = p;
            dataMap = data;
        }

        public void run() {
            NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(mApiClient).await();
            for (Node node : nodes.getNodes()) {

                // Construct a DataRequest and send over the data layer
                PutDataMapRequest putDMR = PutDataMapRequest.create(path);
                putDMR.getDataMap().putAll(dataMap);
                PutDataRequest request = putDMR.asPutDataRequest();
                DataApi.DataItemResult result = Wearable.DataApi.putDataItem(mApiClient,request).await();
                if (result.getStatus().isSuccess()) {
                    Log.v(TAG, "DataMap: " + dataMap + " sent to: " + node.getDisplayName());
                } else {
                    // Log an error
                    Log.v(TAG, "ERROR: failed to send DataMap");
                }
            }
        }
    }

}
