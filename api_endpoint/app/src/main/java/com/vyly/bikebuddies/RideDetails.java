package com.vyly.bikebuddies;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class RideDetails extends AppCompatActivity implements View.OnClickListener {
    private String TAG = "RideDetails";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_details);
        ImageButton home = (ImageButton)findViewById(R.id.rideDetailsButton);


        home.setOnClickListener(RideDetails.this);

        home.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {

            @Override
            public void onSwipeRight() {
//                Intent sendToWatchIntent = new Intent(getApplicationContext(), PhoneSenderService.class);
//                Bundle b = new Bundle();
//                b.putDouble("ride_id", 123456);
//                b.putString("type", PhoneSenderService.START_RIDE_ACTIVITY);
//                sendToWatchIntent.putExtras(b);
//                Log.i(TAG, "send to PhoneSender");
//                getApplicationContext().startService(sendToWatchIntent);
//
//                Intent myIntent = new Intent(RideDetails.this, RideDuringMap.class);
//                Log.i(TAG,"switch to Map" );
//                startActivity(myIntent);
            }
        });

//        home.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                Log.v("ImageButton", "Clicked!");
//                Toast.makeText(RideDetails.this, "ImP", Toast.LENGTH_SHORT).show();
//
//            }
//        });

    }

    private void addButtonListener() {
        ImageButton imgButton = (ImageButton) findViewById(R.id.rideDetailsButton);
        imgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Toast.makeText(RideDetails.this,"ImageButton is working!", Toast.LENGTH_SHORT).show();

            }

        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ride_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        Log.v("ImageButton", "Clicked!");
        Intent sendToWatchIntent = new Intent(getApplicationContext(), PhoneSenderService.class);
        Bundle b = new Bundle();
        b.putDouble("ride_id", 123456);
        b.putString("type", PhoneSenderService.START_RIDE_ACTIVITY);
        sendToWatchIntent.putExtras(b);
        Log.i(TAG, "send to PhoneSender");
        getApplicationContext().startService(sendToWatchIntent);

        Intent myIntent = new Intent(RideDetails.this, RideDuringMap.class);
        Log.i(TAG,"switch to Map" );
        startActivity(myIntent);
//        Toast.makeText(RideDetails.this, "ImP", Toast.LENGTH_SHORT).show();

    }
}
