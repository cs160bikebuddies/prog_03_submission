# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
users = User.create([{name: 'Vy', username: "vy", password: "123456", experience: "Beginner"}, {name: 'Shreya', username: "shreya", password: "123456", experience: "Beginner"},
					{name: 'Matt', username: "matt", password: "123456", experience: "Beginner"}, {name: 'Chris', username: "chris", password: "123456", experience: "Beginner"},
					{name: 'Kevin', username: "kevin", password: "123456", experience: "Beginner"}])
groups = Group.create([{name: 'Hardcore Berkeley Bikers', experience: "Beginner"}, {name: 'Casual Friends', experience: "Beginner"}, {name: 'Berkeley Bike Team', experience: "Beginner"},
					{name: 'My Bike Club', experience: "Beginner"}, {name: 'Friends and Coworkers', experience: "Beginner"}, {name: 'Awesome People', experience: "Beginner"}])

rides = Ride.create([{ride_name: "YOLO", ride_start_time: Time.now + 5.days, length_miles: 2.3, start_location: "Home", dest_location: "Berkeley"},
					{ride_name: "FOOBAR", ride_start_time: Time.now + 10.days, length_miles: 9.1, start_location: "Work",  dest_location: "SF"}])

users.each do |user|
	iter = [1,0,3,1,2].sample
	(1..iter).each do |n|
		user.groups << groups.sample
	end
end

rides[0].user = users[2]
rides[1].user = users[4]

users[0].rides << rides
users[1].rides << rides
users[2].rides << rides[0]
#user[3].rides << rides
users[4].rides << rides[1]

groups[0].rides << rides
groups[1].rides << rides
groups[2].rides << rides
groups[3].rides << rides[1]
groups[4].rides << rides[1]
