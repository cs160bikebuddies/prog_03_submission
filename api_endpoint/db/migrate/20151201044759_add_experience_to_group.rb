class AddExperienceToGroup < ActiveRecord::Migration
  def change
    add_column :groups, :experience, :string
  end
end
