class AddForeignKeysToRide < ActiveRecord::Migration
  	def up
		add_column :rides, :group_id, :integer
		add_column :rides, :user_id, :integer
	end

	def down
  		remove_column :rides, :group_id, :integer
  		remove_column :rides, :user_id, :integer
  	end
end
