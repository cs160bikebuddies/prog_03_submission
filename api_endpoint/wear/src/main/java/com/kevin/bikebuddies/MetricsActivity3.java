package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

public class MetricsActivity3 extends Activity {

    private ImageButton metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics3);

        Intent intent = new Intent(this, BiometricsService.class);
        startService(intent);

        ImageButton metrics = (ImageButton) findViewById(R.id.imageButton);
        metrics.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MetricsActivity3.this, MapActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
