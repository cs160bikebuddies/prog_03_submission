package com.kevin.bikebuddies;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class BiometricsService extends Service {

    private SensorManager mSensorManager;
    private float mAccel; // acceleration apart from gravity
    private float mAccelCurrent; // current acceleration including gravity
    private float mAccelLast; // last acceleration including gravity
    private float velocity;

    private static final int INTERVAL = 60000;
    private static final int SECOND = 1000;

    @Override
    public void onCreate() {
        super.onCreate();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);

        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        createAndStartTimer();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void createAndStartTimer() {

        CountDownTimer timer = new CountDownTimer(INTERVAL, SECOND) {
            public void onTick(long millisUntilFinished) { }
            public void onFinish() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("Tick", "Tock");
                    }
                }).start();

                // Start the timer again
                createAndStartTimer();
            }
        };

        timer.start();
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        float deltaX = 0;
        float deltaY = 0;
        float lastX = 0;
        float lastY = 0;
        float velX = 0;
        float velY = 0;
        float totalVel = 0;
        long timestamp = System.currentTimeMillis() / 1000;

        public void onSensorChanged(SensorEvent se) {
            float x = se.values[0];
            float y = se.values[1];
            float z = se.values[2];
            mAccelLast = mAccelCurrent;
            mAccelCurrent = (float) Math.sqrt((double) (x*x + y*y + z*z));
            float delta = mAccelCurrent - mAccelLast;
            mAccel = mAccel * 0.9f + delta; // perform low-cut filter

            if (mAccelCurrent - mAccelLast > 1) {
                Toast.makeText(getBaseContext(), "Acceleration changed", Toast.LENGTH_SHORT).show();
            }

            if (se.timestamp != timestamp) {
                float deltaTime = se.timestamp - timestamp;

                // get the change of the x,y values of the accelerometer
                deltaX = Math.abs(lastX - se.values[0]);
                deltaY = Math.abs(lastY - se.values[1]);

                // if the change is below 2, it is just plain noise
                if (deltaX < 2)
                    deltaX = 0;
                if (deltaY < 2)
                    deltaY = 0;

                // set the last know values of x,y
                lastX = se.values[0];
                lastY = se.values[1];

                // vx += ax * dt, vy += ay * dt
                velX += lastX * deltaTime;
                velY += lastY * deltaTime;

                totalVel = (float) Math.sqrt(velX + velY);
                velocity = totalVel;

                Log.v("Sensor", String.valueOf(totalVel));
            }
            timestamp = System.currentTimeMillis() / 1000;
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };
}