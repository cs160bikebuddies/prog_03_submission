package com.kevin.bikebuddies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;

public class MetricsActivity1 extends Activity {

    private ImageButton metrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metrics1);

        Intent i = new Intent(this, BiometricsService.class);
        startService(i);

        ImageButton metrics = (ImageButton) findViewById(R.id.imageButton);
        metrics.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()) {
            @Override
            public void onSwipeLeft() {
                Intent myIntent = new Intent(MetricsActivity1.this, MetricsActivity2.class);
                startActivity(myIntent);
            }
        });
    }
}